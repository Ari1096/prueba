﻿**Universidad de San Carlos de Guatemala Facultad de Ingeniería**

**Escuela de Ciencias y Sistemas**

**Software Avanzado**

**Sección N**

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.001.png)

**Proyecto 1 - Fase 1**



|Grupo 10|
| - |
|201612419|Edvin Miguel Castro Ajiatas|
|201612190|Marcos Sebastián Velasquez Cabrera|
|201602730|Pablo César Pacheco Fuentes|
|201520498|Edgar Rolando Herrera Rivas|
Guatemala, 25 de agosto de 2022

**Comunicación de los servicios disponibles para todos los grupos**

Listado de servicios de comunicación externa

Dentro de los requerimientos para la resolución del proyecto es necesario la conectividad con los diferentes equipos de trabajo, dentro los cuales se tocaron temas importantes como los microservicios, entradas y salidas de los mismos. Por lo tanto se tomaran la siguiente consideraciones:

- Encabezados de petición:
- En los headers se va a incluir la identificación del usuario que esté realizado la consulta, así como se adjuntará la llave grupal.
- **Llave común:** c29mdHdhcmVhdmFuemFkb3Byb3llY3RvZmFzZTFzZWNjaW9ubg==
- **Tipo de Petición**: Headers - Token
- **Token:**
- Ejemplo:

\```JSON

{

"group": 7,

"userId": 1,

"email": "",

"name": "",

"region": ""

} ```

- Ver Desarrolladores de Juegos :
- Este microservicio será el encargado de mostrar el listado de los desarrolladores que tengas los juegos.
- **Tipo de Petición**: GET
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200:
- Ejemplo

\```JSON

{

"success": true,

"data": [

{

"developerId": 0, "name": "", "image": ""

}, {

"developerId": 0, "name": "", "image": ""

}

]

} ```

- Código 400:
- Ejemplo:

\```JSON

{

"success": false,

"data": []

}

\```

- Ver Juegos: Este servicio se compone de dos microservicios los cuales son los

siguientes:

- Ver Juegos:
- Este microservicio será el encargado de mostrar los juegos que tengan los distintos grupos
- **Tipo de Petición**: GET
- **Entrada:** Nada
- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200:
- Ejemplo

\```JSON {

"success": true,

"data": [

{

"gameId": 1,

"name": "",

"image": "",

"releaseDate": "DD/MM/YYYY", "restrictionAge": "T",

"price": 0,

"discount": 0,

"group": 1,

"developer": [],

"category": [],

"region": []

},

{ "gameId": 2,

"name": "",

"image": "",

"releaseDate": "DD/MM/YYYY", "restrictionAge": "T",

"price": 0,

"discount": 0,

"group": 1,

"developer": [], "category": [], "region": []

} ]

} ```

- Código 400:

\```JSON {

"success": false, "data": { }

} ```

- Envio de Juegos:
- Este microservicio será el encargado de enviar los juegos a los distintos grupos, para este servicio es necesario tener en cuenta las siguientes categorías:
  - EC - Early ChildHood -  Primera infancia
  - E - Everyone -  Todos
  - E 10+ - Everyone 10 and up -  Todos, mayores de 10 años
  - T - Teen - Adolecentes
  - M - Mature 17+ - Edad Madura
  - AO - Adults only 18+ - Solo para adultos
- **Tipo de Petición**: POST
- **Entrada:**

\```JSON

{

"games": [0,1,...] // gameId

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200:
- Ejemplo

\```JSON

{ "success": true,

"data": [

{

"gameId": 1,

"name": "",

"image": "",

releaseDate": "DD/MM/YYYY", "restrictionAge": "T",

"price": 0,

"discount": 0,

"group": 1, " developer": [],

"category": [],

"region": []

}, {

"gameId": 2, "name": "",

"image": "",

"releaseDate": "DD/MM/YYYY", "restrictionAge": "T",

"price": 0,

"discount": 0,

"group": 1,

"developer": [],

"category": [],

"region": []

}

]

}

\```

- Código 400:

\```JSON {

"success": false, "data": { }

} ```

- Compras de Juegos
- Este microservicio será el encargado de realizar las compras en para los juegos con los distintos equipos de  trabajo.
- **Tipo de Petición**: POST
- **Entrada:**

\```JSON {

[

{

"gameId": 0, "gameName": "", "price": 0, "discount": 0

},

{

"gameId": 0,

"gameName": "",

"price": 0,

"discount": 0,

}

]

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200:
- Ejemplo

\```JSON {

"success": true, "data":

{

"total": 0, "totalWithOffer": 0,

"date": "DD/MM/YYYY HH:MM:SS", "games":

[

{

"name": "",

"offer": "",

"price": 0

}, {

"name": "", "offer": "", "price": 0

} ]

}

}

\```

- Código 400:

\```JSON {

"success": false, "data": { }

} ```

**Diagrama de actividades de tres servicios**

- Servicio de compras y descuentos

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.002.jpeg)

- Servicio de lista de deseos

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.003.jpeg)

- Servicio de Control de Usuario

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.004.jpeg)

**Descripción de la seguridad de la aplicación**

Es muy importante tener un cuenta herramientas que brindarán seguridad a la aplicación con la cual el usuario se podrá sentir seguro a la hora de navegar o realizar sus compras.

- Encriptación de contraseña en base de datos:

La contraseña es muy importante encriptarla ya que existen diversas herramientas en las cuales se pueden extraer la información desde las peticiones, por lo anterior mencionado se utilizará herramientas de encriptación como lo es MD5, esta herramienta es un protocolo criptográfico que se usa para autenticar mensajes y verificar el contenido y las firmas digitales. El MD5 se basa en una función hash que verifica que un archivo que ha enviado coincide con el que ha recibido la persona a la que se lo ha enviado. Anteriormente, MD5 se usaba para el cifrado de datos, pero ahora se utiliza principalmente para la autenticación.

- JSON Web Token (JWT)es un estándar abierto basado en JSON propuesto por IETF para la creación de tokens de acceso que permiten la propagación de identidad y privilegios o claims en inglés. El caso más común de uso de los JWT es para manejar la autenticación en aplicaciones móviles o web. Para esto cuando el usuario se quiere autenticar manda sus datos de inicio de la sesión al servidor, este genera el JWT y se lo manda a la aplicación cliente, luego en cada petición el cliente envía este token que el servidor usa para verificar que el usuario esté correctamente autenticado y saber quién es.

**Documentación de los pipelines**

- Descripción de los distintos stages

Para este caso, se tendrán específicamente 5 stages, que básicamente se encargaran de realizar las tareas más importantes para que la aplicación alojada en el proveedor de nube descrito anteriormente, se despliegue de forma adecuada y vaya registrando y reflejando los cambios realizados por cada uno de los desarrolladores involucrados.

Es importante mencionar que los pipelines serán escritos directamente en la página de gitlab para fines de agilización de procesos.

Los stages definidos son: Auditoría de seguridad, Pruebas unitarias, Build, Container y Deploy.

- Qué se realizará en cada stage
- Auditoría de seguridad Se examinan los paquetes de frontend como backend para buscar algún tipo de vulnerabilidad que afecte el desempeño de la aplicación en general
- Pruebas unitarias En esta sección se ejecutan las pruebas unitarias previamente programadas tanto para la sección de backend como la de frontend, para verificar que el código tenga un funcionamiento lógico y óptimo, y en caso de ser detectada alguna anomalía, realizar la corrección respectiva.
- Build Sección que únicamente se encarga de realizar una nueva compilación del frontend en caso de haber recibido algún cambio
- Container tag & push Todo lo relacionado a la actualización de contenedores y etiquetas para su posterior integración con GKE
- Deploy

La parte más importante del flujo que se encarga de desplegar los nuevos cambios realizados en la aplicación final que se muestra al usuario y el área de servidores que se encargan de la lógica de aplicación

- Explicar el flujo

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.005.png)

Básicamente el flujo inicia con una auditoría a los paquetes tanto de frontend como backend, seguidamente se procede a las pruebas unitarias para verificar el código en su mayoría, y se procede a la parte de Build que por simple analogía aplica únicamente para la parte del frontend, seguidamente se procede a realizar el procedimiento necesario en GKE para actualizar los servidores y por último el despliegue tanto en GKE como App engine de los servidores y frontend.

**Análisis y solución de la problemática**

- Requerimientos:
- El sistema debe ser capaz de registrar usuarios almacenando su información de registro.
- El sistema tiene que enviar un correo de confirmación para los nuevos usuarios que se registren en la plataforma.
- La biblioteca de juegos tiene que ser visible para todos los usuarios, tanto para los usuarios no registrados como para los registrados.
- Se debe implementar un carrito de compras para que los usuarios puedan registrar todos aquellos juegos que deseen comprar.
- El sistema tiene que enviar un correo que informe sobre las transacciones de compra que se estén realizando.
- El sistema tiene que lograr que se muestren los juegos clasificándolos por distintas categorías.
- El sistema tiene que lograr una interconexión con otras plataformas desarrolladas para lograr obtener sus catálogos de juegos y realizar operaciones de compras.
- El sistema tiene que lograr aplicar descuentos y promociones a los distintos juegos y usuarios.
- El sistema tiene que lograr registrar contenido DLC para los juegos.
- Se debe lograr implementar una lista de deseos para los usuarios y puedan registrar los juegos que les han llamado su atención sin necesidad que se compren.
- El administrador debe poder gestionar los usuarios y cualquier incidente registrado con dichos usuarios.
- El administrador tiene que poder gestionar la información de las ofertas, descuentos y promociones.
- Casos de uso de alto nivel

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.006.png)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.007.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.008.png)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.009.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.010.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.011.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.012.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.013.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.014.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.015.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.016.jpeg)

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.017.jpeg)

- Diagrama de arquitectura

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.018.jpeg)

Herramientas de trabajo para:

Gestión de los servicios Servicios de la nube:

Para manejar los servicios y los microservicios que estos conlleva es necesario tenerlo en la nube, en base a lo anterior mencionado se utilizará **“Google Cloud Plataform**” como nube ya que esta plataforma brinda, muchos servicios que se pueden utilizar para el desarrollo de esta aplicación, como por ejemplo el uso de Base de datos, Máquinas virtuales, kubernetes entre otro servicios. Otro factor importante por el cual se escogió Google Cloud Plataform es por la abundante documentación que existe para utilizar dichos servicios y la seguridad que brinda el tener información sensible en sus servicios.

Frameworks

Para el desarrollo de la aplicación se utilizara **“REACT”** ya que existe un una biblioteca completa de componentes para interfaces que pueden ayudar a cumplir con las interfaces solicitadas, así como su uso fácil para la implementación. Este Framework está basado en componentes encapsulados esto quiere decir que manejan su propio estado y se pueden convertir en interfaces de usuario más complejas.

DBMS

El motor de la base de datos que se implementara para el proyecto es **“MYSQL”** esto se debe a la gran velocidad que nos brinda mysql, la seguridad sobre los datos, el manejo estándar de lenguaje, su versatilidad sobre los sistemas operativos, soporta bases de datos de de gran tamaño y sobre todo que es gratis.

Base de datos

- Diagrama ER

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.019.jpeg)

Gestión del proyecto

**Herramientas para el manejo de la documentación**

- **Datamodeler:** Al establecer que se utilizará una base de datos relacional, se decidió utilizar esta herramienta para el modelado del diagrama entidad relación de la plataforma. Datamodeler nos permite crear diagramas lógicos de un sistema de datos relacional, por medio de entidades y relaciones. Además de que al ser una herramienta gratuita, y de pocos requerimientos, permite un fácil acceso a los usuarios al momento de utilizarla.
- **Visual Studio Code:** El editor de texto utilizado para la escritura de código en formato Markdown. Su interfaz es bastante completa, además de que tiene la característica de incorporar extensiones para complementar y facilitar el trabajo. La extensión Markdown preview, permite visualizar los cambios hechos en el archivo .md antes de realizar un commit.
- **Draw io:** Esta herramienta fue seleccionada para realizar los diagramas, ya que con una cuenta gratuita, es posible acceder a los elementos necesarios para crear diagramas muy bien estructurados. Además, se puede hacer uso de ella desde cualquier navegador sin la necesidad de instalar algún tipo de software.Finalmente, con esta herramienta se pueden exportar los diagramas al formato deseado de acuerdo a la presentación que se desee.

**Tablero de control**

El tablero de control es una herramienta gerencial que tiene por objetivo principal presentar el estado actual de una o varias tareas. De esta forma es posible gestionar mejor la toma de decisiones dentro de un equipo de trabajo. Para trabajar el tablero de control de este proyecto, el equipo de desarrollo optó por utilizar Trello.

**Trello**

Es un software de administración de proyectos con interfaz web y con cliente para iOS y android para organizar proyectos. Con esta herramienta es posible diseñar planes y organizar flujos de trabajo, mejorando el seguimiento del progreso.

- Se eligió debido a que cuenta con muchas ventajas:
- Su plan de cuentas gratuito provee las herramientas necesarias para monitorear el proyecto de manera eficiente.
- Sistema de trabajo colaborativo.
- Permite organizar tareas con etiquetas.
- Es multiplataforma, por lo que se puede visualizar el tablero en variedad de dispositivos
- Su interfaz gráfica es amigable y fácil de usar.
- Los cambios realizados se ven reflejados en tiempo real para todos los miembros del equipo.

Comunicación entre servicios

**Comunicación HTTP**

Para el desarrollo de este proyecto se utilizó la comunicación HTTP para poder comunicar cada uno de los servicios que componen la plataforma. Así mismo, es la mejor manera de comunicar al cliente final con todos los servicios y rutas necesarias para el funcionamiento del proyecto.

Los mensajes HTTP, son los medios por los cuales se intercambian datos entre servidores y clientes. Hay dos tipos de mensajes: peticiones, enviadas por el cliente al servidor; y respuestas, que son la respuesta del servidor.

Las peticiones HTTP son mensajes enviados por un cliente, para iniciar una acción en el servidor. Su línea de inicio está formada por tres elementos:

- Un método HTTP, un verbo como: GET, PUT o POST)
- El objetivo de una petición, normalmente es una URL
- La versión de HTTP, la cual define la estructura de los mensajes y de las respuestas

**Tipos de peticiones utilizadas**

Para implementar todas las funcionalidades del proyecto, se manejan dos tipos de peticiones:

- GET: El método GET  solicita una representación de un recurso específico. Las peticiones que usan el método GET sólo deben recuperar datos.
- POST: El método POST se utiliza para enviar una entidad a un recurso en específico, causando a menudo un cambio en el estado o efectos secundarios en el servidor.

Versionamiento

Para el manejo de versiones del proyecto se estará utilizando **“GitLab**” ya que esta herramienta cuenta con varias propiedades que se pueden utilizar para las necesidades del proyecto, como es la integración continua, Criterios de aceptación, entre otras. GitLab es una plataforma de gestión de ciclo de vida DevOps, integrada y de código abierto, dirigida a que los equipos de desarrollo de software planifiquen, programen, prueben, implementen y supervisen cambios en productos dentro de una sola aplicación. GitLab permite que los usuarios optimicen sus flujos de trabajo colaborativos y visualicen, prioricen, coordinen y lleven un seguimiento del progreso utilizando herramientas flexibles de gestión de proyectos.

Criterios de aceptación

- ver como está bien el código
- ver si están bien las tareas
- ramas trabajadas correctamente

**Descripción de microservicios**

**Modulo de Compras y Descuentos:**

Para este módulo se manejan distintos endpoint en el cual nos ayudará a tener a la mano como las promociones por regiones, por fecha entre otras promociones, así como crear promociones para ciertos juegos y los descuentos que estos pueden aplicar.

**Microservicios**

- **Mostrar descuentos por juego**: este microservicio será el encargado de mostrar el descuento para ver si aplica a los juegos que se estén mostrando en ese momento
- **Historia de usuario:**
  - **“El usuario final necesita conocer los descuentos que hay por los diferentes juegos de modo aprovechar los descuentos que tengan los juegos”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición**: POST
- **Entrada: i**djuego
- Ejemplo:

\```JSON

{

idjuego: 125

}

- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200: Éxito, retorna la información del porcentaje de descuento por juego .
- Ejemplo

\```JSON

{

“idDescuento”: 12,

“Nombre”: “Descuento en juego de Guerra”

“Observacion”: “Todos los juegos que incluyan guerra se les hara el descuento”

“Porcentaje”: “25%”

}

\```

- Código 400: Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se pudo consultar a base de datos” }

\```

- Validaciones Requeridas: No es necesario ningún tipo de de validación.
- **Mostrar descuento por región:** este microservicio será el encargado de mostrar el descuento para ver si aplica a los juegos que se estén mostrando en ese momento
- **Historia de usuario:**
  - **“El usuario final necesita conocer los descuentos que hay por región de modo que pueda aprovechar los descuentos que estén en su región. ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** idjuego, idregion
- Ejemplo

\```JSON

{

“idJuego”: 12, “idRegion”: 20

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- Código 200: Éxito, retorna la información del porcentaje de descuento por juego .
- Ejemplo

\```JSON

{

“idDescuento”: 12,

“Nombre”: “Descuento en juego de Guerra”

“Observacion”: “Todos los juegos que incluyan guerra se les hara el descuento”

“Porcentaje”: “25%”

}

\```

- Código 400: Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se pudo consultar a base de datos” }

\```

- Validaciones Requeridas: No es necesario ningún tipo de de validación.
- **Mostrar descuentos en general:** este endpoint retorna todos los descuentos que estén disponibles
- **Historia de usuario:**
  - **“El usuario final necesita conocer los descuentos que hay dentro de la aplicación de modo que pueda verificar si le conviene uno u otro. ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** GET
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna la información del porcentaje de descuento por juego .
- Ejemplo

\```JSON

{

“idDescuento”: 12,

“Nombre”: “Descuento en juego de Guerra”

“Observacion”: “Todos los juegos que incluyan guerra se les hara el descuento”

“Porcentaje”: “25%”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON {

“Mensaje”: “No se pudo consultar a base de datos”

} ```

\```

- **Validaciones Requeridas**: No es necesario ningún tipo de de validación.
- **Mostrar Promociones:** este microservicio retornará todas las promociones que estén disponibles
- **Historia de usuario:**
  - **“El usuario final necesita conocer las promociones en general de modo que pueda verificar si le conviene alguno en específico ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** GET
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna la información de la promocion que se pueda aplicar
- Ejemplo

\```JSON {

“idPromocion”: 12,

“Nombre”: “Promocion de Deportes”

“Observación”: “Todos los juegos de Deportes  a 2x1”

} ```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON {

“Mensaje”: “No se pudo consultar a base de datos”

}

\```

- **Validaciones Requeridas**: No es necesario ningún tipo de de validación.
- **Promoción por Juego:** Mostrará las promociones que pueda aplicar el juego seleccionado
- **Historia de usuario:**
  - **“El usuario final necesita conocer las promociones por juegos de modo pueda verificar si puede aplicar o le conviene alguno en específico.  ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentacion**
- **Tipo de Petición:** POST
- **Entrada:** idJuego
- Ejemplo:

\```JSON

{

“idjuego”: 125

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna la información de la promoción que se pueda aplicar
- Ejemplo

\```JSON

{

idPromocion: 12,

Nombre: “Promocion de Deportes”

Observación: “Todos los juegos de Deportes  a 2x1” }

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se pudo consultar a base de datos” }

\```

- **Validaciones Requeridas**: No es necesario ningún tipo de de validación.
- **Promoción por región:** este microservicio mostrara si la región tiene alguna promoción en especial
- **Historia de usuario:**
  - **“El usuario final necesita conocer las promociones que hay por región de modo que pueda aprovechar las promociones que estén en su región. ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentacion**
- **Tipo de Petición:** POST
- **Entrada:** idJuego, idRegion
- Ejemplo:

\```JSON

{

“idjuego”: 125,

“idRegion”: “Norte”

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna la información de la promoción que se pueda aplicar
- Ejemplo

\```JSON

{

idPromocion: 12,

Nombre: “Promocion de Deportes”

Observación: “Todos los juegos de Deportes  a 2x1” }

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se pudo consultar a base de datos” }

\```

- **Validaciones Requeridas**: No es necesario ningún tipo de de validación.

**Módulo de Lista de deseos:**

Este módulo será el encargado de registrar o eliminar la lista de deseos del usuario, los microservicios que se van a manejar son los siguientes:

- **Agregar juego a listado:** este microservicio será el encargado de registrar en la lista de deseos el juego seleccionado por el usuario
- **Historia de usuario:**
  - **“El usuario final necesita agregar un juego que le haya llamado la atención o gustado de modo de tener un listado para que en un futuro lo pueda comprar”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentacion**
- **Tipo de Petición:** POST
- **Entrada:** idJuego
- Ejemplo:

\```JSON

{

“idjuego”: 125,

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de agregado con exito
- Ejemplo

\```JSON

{

“Mensaje” :”Agregado con Éxito” }

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede agregar a favoritos” }

\```

- **Validaciones Requeridas**: Que el usuario esté registrado



- **Eliminar del listado de deseos:** este microservicio será el encargado de eliminar el juego de la lista de deseos del usuario.
- **Historia de usuario:**
  - **“El usuario final necesita eliminar un juego de su lista de deseo de modo ya no tenerlo en sus preferencias de juegos”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** idJuego
- Ejemplo:

\```JSON

{

“idjuego”: 125,

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un mensaje de éxito
- Ejemplo

\```JSON

{

“Mensaje” :”Eliminado  con Éxito” }

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede eliminar de  favoritos” }

\```

- **Validaciones Requeridas**: Que el usuario esté registrado
- **Eliminar por completo la lista de deseos**: este microservicio eliminará todos los juegos que estén en el listado no importando cual sea
- **Historia de usuario:**
  - **“El usuario final necesita eliminar toda la lista de deseos de modo a que ya no visualice los juegos anteriores que le haya gustado. ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** idUsuario
- Ejemplo:

\```JSON

{

“idUsuario”: 15,

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de exito
- Ejemplo

\```JSON

{

“Mensaje” :”Eliminado  con Éxito” }

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede eliminar de  favoritos” }

\```

- **Validaciones Requeridas**: Que el usuario esté registrado

**Modulo de Control de Usuario:**

Este módulo será el encargado de desactivar los usuarios que estén reportados, así como el de agregar las fechas de algunas promociones:

- **Crear reporte:** este microservicio será el encargado de registrar el reporte de un jugador
- **Historia de usuario:**
  - **“El usuario debe tener la posibilidad de reportar usuarios que tengan un comportamiento inapropiado”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** Usuario que crea el reporte, usuario reportado, comentario, fecha
- Ejemplo:

\```JSON

{

“idUsuario1”: 15, “idUsuario2”: 10,

“fecha”: “DD/MM/YYY”, “comentario”: “”

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de exito
- Ejemplo

\```JSON

{

“Mensaje” :”Reportado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede reportar este usuario” }

\```

- **Validaciones Requeridas**: Que ambos usuarios existan
- **Desactivar Usuario:** este microservicio será el encargado de cambiar el estado al usuario y así no podrá ingresar a la plataforma
- **Historia de usuario:**
  - **“El usuario administrador necesita poder desactivar usuarios que hayan sido reportados de modo a que el usuario reportado ya no pueda acceder a la aplicación”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** idUsuario
- Ejemplo:

\```JSON

{

“idUsuario”: 15,

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de exito
- Ejemplo

\```JSON

{

“Mensaje” :”Deshabilitado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede deshabilitar este usuario” }

\```

- **Validaciones Requeridas**: Que el usuario haya sido reportado
- **Agregar Promoción:** este microservicio será el encargado de agregar una nueva promoción
- **Historia de usuario:**
  - **“El usuario administrador necesita agregar una nueva promoción de modo que esta promoción pueda ser visible para los demás usuarios ”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentacion**
- **Tipo de Petición:** POST
- **Entrada:** idUsuario
- Ejemplo:

\```JSON

{

“Nombre”: , “Propocion del dia del amor”, “Descipcion”:”Por el mes de Febrero”, “Promocion”:”Llevate una uan sopresa”

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de éxito
- Ejemplo

\```JSON

{

“Mensaje” :”Agregado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede agregar la promocion”

}

\```

- **Validaciones Requeridas**: Que sea el usuario administrador que agregue la

promoción

- **Eliminar Promoción:** este microservicio será el encargado de cambiar el estado a la promoción en desactivado.
- **Historia de usuario:**
  - **“El usuario administrador necesita eliminar una promoción de modo que esta promoción no pueda ser visible para los demás usuarios”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentacion**
- **Tipo de Petición:** POST
- **Entrada:** idUsuario
- Ejemplo:

\```JSON

{

“idDescuento”: 2

{

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de éxito
- Ejemplo

\```JSON

{

“Mensaje” :”Eliminado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede eliminar la promoción”

}

\```

- **Validaciones Requeridas**: Que sea el usuario administrador que elimine el

descuento

- **Agregar Descuentos:** este microservicio se utilizará para agregar nuevos descuentas que tenga la empresa
- **Historia de usuario:**
  - **“El usuario administrador necesita agregar un nuevo descuento de modo que este descuento pueda ser visible para los demás usuarios”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **Entrada:** idUsuario
- Ejemplo:

\```JSON

{

“Nombre”: , “Descuento”,

“Descripción”:”Por el mes de navidad”, “Descuento”: 25%

}

\```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de éxito
- Ejemplo

\```JSON

{

“Mensaje” :”Agregado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo: ```JSON

“Mensaje”: “No se puede agregar la promocion”

}

\```

- **Validaciones Requeridas**: Que sea el usuario administrador que agregue la

Descuento

- **Eliminar Descuento:** este microservicio será el encargado de cambiarle el estado a las promociones a un estado deshabilitado
- **Historia de usuario:**
  - **“El usuario administrador necesita eliminar un descuento de modo que este descuento no pueda ser visible para los demás usuarios”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** GET
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna mensaje de éxito
- Ejemplo

\```JSON

{

“Mensaje” :”Eliminado  con Éxito”

}

\```

- **Código 400:** Error, retorna un mensaje de error
- Ejemplo:

\```JSON

{

“Mensaje”: “No se puede eliminar la promoción”

}

\```

- **Validaciones Requeridas**: Que sea el usuario administrador que elimine el

descuento

**Módulo Juegos:**

Este módulo es el encargado de visualizar los juegos y dlc por región.

- **Ver juegos por región:** este microservicio será el encargado de devolver la información de todos los juegos disponibles por región.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la información de los juegos disponibles en su región”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST


- **URL:** juegosUsuario
- **Entrada:** región del usuario
- Ejemplo
- Ejemplo:

\```JSON {

"region": “”

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los

juegos disponibles en la region

- Ejemplo

\```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}, {

"gameId": 2, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo: ```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos.

- **Ver dlc:** este microservicio será el encargado de devolver la información de todos los dlc disponibles por región.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la información de los dlc disponibles en su región”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **URL:** dlcUsuario
- **Entrada:** región del usuario
- Ejemplo
- Ejemplo:

\```JSON {

"region": “”

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los

juegos disponibles en la region

- Ejemplo

\```JSON

{

"success": true, "data": [

{

"game": “”, "name": "", "description": "",

}, {

"game": “”,

"name": "",

"description": "", }

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo: ```JSON

{

"success": false, "data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos.

**Módulo Catálogo:**

Este módulo será el encargado para poder visualizar todos los juegos y contenido de la plataforma aplicando diferentes filtros:

- **Ver juegos:** este microservicio será el encargado de devolver la información de todos los juegos que se encuentren en la plataforma.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar los juegos disponibles en la plataforma, incluyendo todos sus datos.”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** GET
- **URL:** games
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los juegos
- Ejemplo

\```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.

- Ejemplo:

\```JSON

{

"success": false, "data": { }

}

\```

- **Validaciones Requeridas**: Ninguna
- **Ver juegos 2:** este microservicio será el encargado de devolver la información de todos los juegos solicitados.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la información de los juegos solicitados por medio de un array.”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **URL:** games
- **Entrada:** identificadores de los juegos
- Ejemplo
- Ejemplo:

\```JSON {

"games": [0,1,...]

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los

juegos solicitados

- Ejemplo ```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}, {

"gameId": 2,

"name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0,

"discount": 0,

"group": 1,

"developer": [], "category": [],

"region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo:

\```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos. El array debe de contener números.

- **Ver juegos por género:** este microservicio será el encargado de devolver la información de todos los juegos de acuerdo al género indicado.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la información de los juegos solicitados por género”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **URL:** juegosGenero
- **Entrada:** nombre del género
- Ejemplo
- Ejemplo:

\```JSON {

"nombre": “deportes”

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los juegos que tengan el género indicado
- Ejemplo

\```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}, {

"gameId": 2, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0,

"discount": 0,

"group": 1,

"developer": [], "category": [],

"region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo:

\```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos.

- **Ver juegos por desarrollador:** este microservicio será el encargado de devolver la información de todos los juegos de acuerdo al desarrollador indicado.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la información de los juegos solicitados por desarrollador”**
- **Criterio de aceptación:**
- **TEST técnico**
- **Documentación**


- **Tipo de Petición:** POST
- **URL:** juegosDesarrollador
- **Entrada:** nombre del desarrollador
- Ejemplo
- Ejemplo:

\```JSON {

"nombre": “ubisoft”

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los juegos que tengan el desarrollador indicado
- Ejemplo

\```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}, {

"gameId": 2, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo: ```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos..

- **Ver recomendaciones:** este microservicio será el encargado de devolver la información de todos los juegos que contengan géneros de videojuegos comprados por el usuario.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la juegos que la plataforma le recomiende en base a sus compras anteriores”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** POST
- **URL:** recomendaciones
- **Entrada:** Id del usuario
- Ejemplo
- Ejemplo:

\```JSON {

"id":1

} ```

- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de los juegos que recomienda la plataforma
- Ejemplo

\```JSON

{

"success": true, "data": [

{

"gameId": 1, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0, "discount": 0, "group": 1, "developer": [], "category": [], "region": []

}, {

"gameId": 2, "name": "", "image": "", "releaseDate":

"DD/MM/YYYY",

"restrictionAge": "T", "price": 0,

"discount": 0,

"group": 1,

"developer": [], "category": [],

"region": []

}

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo:

\```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: El body de la petición POST debe contener todos

los campos requeridos.

- **Ver calificaciones:** este microservicio será el encargado de devolver la información de todas las reseñas de los juegos.
- **Historia de usuario:**
  - **“El usuario necesita poder visualizar la las reseñas y calificaciones de los otros usuarios en la plataforma”**
- **Criterio de aceptación:**
  - **TEST técnico**
  - **Documentación**
- **Tipo de Petición:** GET
- **URL:** calificaciones
- **Entrada:** Ninguna
- **Salida**: Para ambas salidas que se manejan será un JSON
- **Código 200**: Éxito, retorna un JSON con la información de las

reseñas en la plataforma

- Ejemplo

\```JSON

{

"success": true, "data": [

{

"id": 1, "user": "", "game": "", "estrellas": 4, "comentario": ""

},

{

"id": 2,

"user": "",

"game": "",

"estrellas": 5,

"comentario": "" }

]

}

\```

- **Código 400:** Error, un estado false y ninguna data.
- Ejemplo:

\```JSON

{

"success": false,

"data": { }

}

\```

- **Validaciones Requeridas**: Ninguna.

**Diagrama de servicios**

- Servicio de Compras y descuentos

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.020.jpeg)

- Servicio Lista de deseos

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.021.png)

- Servicio de Control de usuario

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.022.jpeg)

- Servicio de Juegos

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.023.png)

- Servicio de Catálogo

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.024.jpeg)

**Bitácora de las tareas**

- Descripción de la herramienta

Para registro de tareas se utilizó la herramienta “Trello”, este software permite ir manejando tableros virtuales para realizar manipulación de tarjetas digitales en las cuáles se van a registrar las tareas a realizar. Permite que se le designen a los usuarios invitados a trabajar al tablero, agregarle una des

- ¿por qué se utilizó?
- Pros y contras
- Proceso scrum
  - Sprint planning

Se realizó el día 20 de agosto del 2022 y se trataron los siguientes puntos:

- Se planificaron las fechas para realizar los daily scrum
- Se realizó el desglose de actividades a completar
- Se designaron las tareas a los miembros del equipo

Tablero inicial:

![](Aspose.Words.edbb940c-59d2-4177-a15e-4feb38747213.025.jpeg)

Tablero completo:

- Daily scrum:

Daily del 21 de agosto:

Se trataron los siguientes puntos:

- Edvin
- ¿Qué se realizó ayer?
  - Se realizó la asignación de tareas en el sprint planning y se definieron las entregas a realizar.
- ¿Qué voy a hacer hoy?
  - Documentarse respecto a las tareas que me fueron asignadas para desempeñarlas correctamente
- Inconvenientes
- Sin inconvenientes
- Edgar
- ¿Qué se realizó ayer?
  - Se realizó la asignación de tareas en el sprint planning y se definieron las entregas a realizar.
- ¿Qué voy a hacer hoy?
  - Buscar información respecto a las tareas que se me asignaron.
- Inconvenientes
- No tengo inconveniente por el momento
- Sebas
- ¿Qué se realizó ayer?
  - Se realizó la asignación de tareas en el sprint planning y se definieron las entregas a realizar.
- ¿Qué voy a hacer hoy?
  - Buscar herramientas para modelar el diagrama ER de la base de datos relacional.
- Inconvenientes
  - Por el momento no he tenido inconvenientes
- Pablo
- ¿Qué se realizó ayer?
  - Se realizó la asignación de tareas en el sprint planning y se definieron las entregas a realizar.
- ¿Qué voy a hacer hoy?
  - Se agregará la información sobre la selección del marco de trabajo a utilizar para trabajar.
- Inconvenientes
- No se tienen inconvenientes por el momento.

Daily del 23 de agosto:

Se trataron los siguientes puntos:

- Edvin
- ¿Qué se realizó ayer?
  - Se inició con la elaboración de los diagramas de actividad solicitados para los 3 servicios
- ¿Qué voy a hacer hoy?
  - Finalizar la elaboración de los diagramas de actividades y proceder a elaborar el diagrama de arquitectura
- Inconvenientes
- No se comprendía a cabalidad el flujo de trabajo del diagrama de actividades
- Edgar
- ¿Qué se realizó ayer?
  - Se trabajó sobre descripciones de los microservicios de los distintos módulos.
- ¿Qué voy a hacer hoy?
  - Complementar con las historias de usuarios respecto a los módulos.
- Inconvenientes
- De momento no se tiene ningún inconveniente
- Sebas
- ¿Qué se realizó ayer?
  - Se realizó el diagrama ER de la base de datos
- ¿Qué voy a hacer hoy?
  - Trabajar en las descripciones de los microservicios de los módulos asignados
- Inconvenientes
- Por el momento no he tenido inconvenientes
- Pablo
- ¿Qué se realizó ayer?
  - Se agregó la información sobre la selección del marco de trabajo a utilizar
- ¿Qué voy a hacer hoy?
  - Se listarán los requerimientos y los casos de uso de alto nivel.
- Inconvenientes
- Por el momento no se tienen inconvenientes

Daily del 24 de agosto:

Se trataron los siguientes puntos:

- Edvin
- ¿Qué se realizó ayer?
  - Se finalizó con el diagrama de arquitectura y se procedió a iniciar el diagrama de servicios
- ¿Qué voy a hacer hoy?
  - Iniciar con la documentación de los pipelines
- Inconvenientes
- Incertidumbre sobre los stages mínimos que debería llevar el pipeline
- Edgar
- ¿Qué se realizó ayer?
  - Complementar las historias de usuarios faltantes del día de ayer
- ¿Qué voy a hacer hoy?
  - Se agregó la descripción respecto a las metodología Ágil
- Inconvenientes
- De momento no tengo inconveniente
- Sebas
  - ¿Qué se realizó ayer?
    - Se terminó la descripción de los microservicios de los módulos asignados
  - ¿Qué voy a hacer hoy?
    - Documentar
  - Inconvenientes
- Pablo
  - ¿Qué se realizó ayer?
  - ¿Qué voy a hacer hoy?
  - Inconvenientes
- Describir las tareas que se realizaron
  - Tareas principales
  - Subtareas (explicar esta subdivisión)
- Historias de usuario
- Herramienta utilizada
- LInk:
- https://trello.com/b/s8EUrJqi/saproyectof1grupo10
- Peso
- Tiempo ●

**Metodología ágil utilizada: Marco de trabajo SCRUM**

- Explicación del porqué se seleccionó SCRUM:

Se eligió SCRUM debido a que es una metodología bastante eficaz que permite obtener resultados rápidamente. Al estar trabajando en equipo para solventar la problemática que se nos presenta, ésta metodología se adapta bastante bien a la organización ya que utiliza equipos multifuncionales para poder desarrollarse y además fomenta la auto-organización de los miembros para completar las tareas propuestas, éste empoderamiento permite ser más creativos para completar las distintas entregas planeadas.

Al ser una metodología ágil nos permite obtener resultados rápidamente y en un plazo de tiempo relativamente corto, por lo cual el tiempo se aprovecha de una mejor manera y se puede avanzar en el desarrollo del proyecto con mayor velocidad. Lo cual es sumamente importante ya que es necesario que se aproveche al máximo los espacios de tiempo otorgados y con Scrum es posible optimizar la ventana de tiempos al planificar correctamente cada sprint y así completar las distintas entregas.

Como equipo nos permite cooperar con los demás para que podamos llegar a la meta que se establece con anterioridad, ésto debido a que Scrum es transparente por lo que se puede observar cómo va cada miembro, y si algunos van atrasados nos permite reorganizar las tareas para avanzar en el progreso de los entregables que se han designado.

- Etapas respectivas:

Para plasmar las las necesidades que se le asignaron a los miembros se utilizó un tablero de historias en las cuales mediante tarjetas se colocaron los entregables que debían ser trabajados y varias etapas que permiten verificar la manera en que van desarrollándose y cuánto progreso se tiene. Se agregaron las siguientes columnas de progreso:

- Product Backlog.

Esta columna representa la planificación que se realizó en el sprint, almacenando la lista de tareas que hay por hacer.

- To Do

Esta columna representa cuando una tarea ya ha sido asignada a un miembro del equipo y se encuentra lista para empezar a trabajarse.

- In Progress

Esta columna indica cuando ya se ha iniciado a trabajar la tarea designada.

- Done

Esta columna almacenará la tarjeta correspondiente a las tareas que ya se han completado.

Dentro del marco de trabajo SCRUM existen varios artefactos o herramientas que se pueden utilizar para llevar a cabo una buena metodología ágil. Las herramientas que se utilizaran para este proyecto son:

- Product Backlog:

Es un repositorio que contiene cualquier tipo de trabajo que se necesita para que se lleve a cabo el proyecto o producto. Este repositorio es la principal fuente de información del proyecto en SRUM. Dentro del Backlog el autor prinpal es el Producto Owner el cual es la persona encargada de darle la prioridad a las tareas que se encuentran en él. Dentro de las que se puede agregar al backlog son las siguientes:

- Requerimientos
- Casos de Usos
- Tareas
- Sprint Backlog

Se trata de un listado de tareas que se van a manejar dentro de determinado tiempo llamado Sprint, estas tareas normalmente se componen de tareas técnicas más pequeñas que permiten conseguir un incremento en el proyecto y poder alcanzar el objetivo del sprint.

- Incremento o Resultado de Sprint

El incremento es el resultado de cada sprint que se realice en otras palabras es una parte del proyecto ya terminada, este resultado busca aportar un nuevo valor al proyecto que se está desarrollando.

Para llevar a cabo una buen marco de trabajo scrum es necesario tener en cuenta los siguiente eventos:

- Sprint: Es una cantidad de tiempo de trabajo en específico por lo regular es de 2 semanas, este evento  busca delimitar en tareas que se puedan cumplir dentro de este tiempo específico.
- Sprint Planning: Este evento busca planificar las tareas que sean necesarias para cumplir el objetivo de un sprint
- Daily Scrum: son reuniones cortas en las cuales se comprueba el estado de las tareas planificadas en el Sprint Planning.
- Sprint Review: Este evento tiene como objetivo dar a conocer las tareas finalizadas como las tareas que quedaron pendientes dentro del sprint.
- Sprint Retrospective: Busca dar una retroalimentación sobre un sprint terminado, en este se discute sobre los exito o dificultades que se tuvieron dentro del sprint.
